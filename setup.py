#!/usr/bin/env python2
# -*- coding: utf-8 -*-
from setuptools import setup
from os import path

__author__ = 'Kien Truong'
here = path.abspath(path.dirname(__file__))

setup(
    name='spok-client',
    version='0.1.0',
    description='Port knocking client for network security project',
    long_description='Port knocking client for network security project',
    author='Team KKN',
    license='MIT',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2.7'
    ],
    keywords='port-knocking security',
    packages=['spok_knocker'],
    install_requires=['scapy==2.2.0-dev', 'pyotp'],
    data_files=[('sample_config', ['config.client.ini', 'log_config.json'])],
    entry_points={
        'console_scripts': [
            'spok_client=spok_knocker.main:main',
        ],
    },
)
