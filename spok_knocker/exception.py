#!/usr/bin/env python2
# -*- coding: utf-8 -*-

__author__ = 'Kien Truong'


class InvalidKeyException(Exception):
    pass


class InvalidConfigurationException(Exception):
    pass