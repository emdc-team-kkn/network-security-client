#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# Supress scapy logging
import logging
from spok_knocker.exception import InvalidConfigurationException, \
    InvalidKeyException
from spok_knocker.keytool import derive_key, validate_key

logging.getLogger("scapy.runtime").setLevel(logging.ERROR)

import pyotp
from scapy.layers.inet import TCP, IP
from scapy.all import *
import time
import ConfigParser
import datetime

__author__ = 'Kien Truong'

# Module level variable
log = logging.getLogger(__name__)


class KnockConfig(object):
    """
    This class represent one knock configuration.
    """
    def __init__(self):
        self.sequence = []
        self.protocol = 1
        self.sequence_size = 0
        self.name = None
        self.key = None
        self.secondary_key = None

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return "KnockConfig: ({name}, {sequence}, {key}, " \
               "{sequence_size}, {protocol}) "\
            .format(
                sequence='|'.join((str(i) for i in self.sequence)),
                name=self.name,
                key=self.key,
                sequence_size=self.sequence_size,
                protocol=self.protocol
            )


class KnockConfigP1(KnockConfig):
    """
    This class represent a protocol 1 KnockConfiguration.
    """
    def __init__(self):
        super(KnockConfigP1, self).__init__()
        self.protocol = 1

    def execute(self, destination, interval=1):
        if self.key is not None:
            # Secure Protocol 1
            totp_gen_checksum = pyotp.TOTP(self.secondary_key)
            current = totp_gen_checksum.timecode(datetime.datetime.now())
            for i in range(0, self.sequence_size):
                checksum_code = '%06d' % \
                                totp_gen_checksum.generate_otp(current + i)
                port = self.sequence[i]
                log.debug("TOTP checksum code: %s", checksum_code)
                knock_port(destination, port, checksum_code)
                time.sleep(interval)

            # Last port are generated from totp
            port_code = pyotp.TOTP(self.key).now()
            log.debug("TOTP port code: %s", port_code)
            log.debug("TOTP checksum code: %s", checksum_code)
            port_code = int(port_code) % 65536
            checksum_code = '%06d' % \
                totp_gen_checksum.generate_otp(current +
                                               self.sequence_size)
            knock_port(destination, port_code, checksum_code)
        else:
            # Normal port knocking protocol
            for port in self.sequence:
                knock_port(destination, port)
                time.sleep(interval)


class KnockConfigP2(KnockConfig):
    """
    This class represent a protocol 2 KnockConfiguration.
    """
    def __init__(self):
        super(KnockConfigP2, self).__init__()
        self.protocol = 2

    def execute(self, destination, interval=1):
        totp_gen_port = pyotp.TOTP(self.key)
        totp_gen_checksum = pyotp.TOTP(self.secondary_key)
        current_timecode = totp_gen_port.timecode(datetime.datetime.now())
        for i in range(0, self.sequence_size):
            port_code = totp_gen_port.generate_otp(current_timecode + i)
            checksum_code = '%06d' % \
                            totp_gen_checksum.generate_otp(current_timecode + i)
            log.debug("TOTP port code: %s", port_code)
            log.debug("TOTP checksum code: %s", checksum_code)
            totp_port = int(port_code) % 65536
            knock_port(destination, totp_port, checksum_code)
            time.sleep(interval)


def read_knock_configs(config_file_path):
    """
    This function read a configuration file and
    return a mapping of name -> KnockConfig
    """
    config_parser = ConfigParser.ConfigParser()
    read_oks = config_parser.read(config_file_path)

    if not read_oks:
        raise IOError("Unable to read configuration file " + config_file_path)

    sections = config_parser.sections()
    output = {}
    for section in sections:
        if (config_parser.has_option(section, 'protocol') and
                config_parser.get(section, 'protocol') == '2'):
            kconfig = KnockConfigP2()
        else:
            kconfig = KnockConfigP1()
        kconfig.name = section
        if config_parser.has_option(section, 'sequence'):
            sequence = config_parser.get(section, 'sequence').split(',')
            for i in sequence:
                kconfig.sequence.append(int(i))
        if config_parser.has_option(section, 'key'):
            key = config_parser.get(section, 'key')
            try:
                validate_key(key)
            except InvalidKeyException, ex:
                message = "Section {s} doesn't have a valid key"\
                    .format(s=section)
                print message
                raise ex
            kconfig.key = key
            kconfig.secondary_key = derive_key(key)
        else:
            message = "Section {s} doesn't have a valid key".format(s=section)
            raise InvalidConfigurationException(message)
        if config_parser.has_option(section, 'sequence_size'):
            kconfig.sequence_size = int(
                config_parser.get(section, 'sequence_size')
            )
        else:
            kconfig.sequence_size = len(kconfig.sequence)
        output[section.lower()] = kconfig
    return output


def knock_port(destination, port, checksum_code=None):
    # Build the TCP packet
    tcp_packet = TCP()
    tcp_packet.dport = port
    if checksum_code is not None:
        tcp_packet.payload = checksum_code

    # Build the IP packet
    ip_packet = IP()
    ip_packet.dst = destination
    ip_packet.payload = tcp_packet

    # Send it
    log.debug("Knocking: %s:%d", destination, port)
    send(ip_packet, verbose=False)