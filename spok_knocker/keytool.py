#!/usr/bin/env python2
# -*- coding: utf-8 -*-

__author__ = 'Kien Truong'

from spok_knocker.exception import InvalidKeyException
import random
import base64
import hashlib

# Minimum key size in bytes to use in our project
MIN_KEY_SIZE = 20
SMALL_KEY_MSG = "The key size is too small, " \
                "it must be at least {size} bytes.".format(size=MIN_KEY_SIZE)
INVALID_KEY_MSG = "The key cannot be decoded."


def gen_key(keysize):
    """
    This method generate the key with size specified in bytes.
    The key should not be smaller than MIN_KEY_SIZE.
    """
    if keysize < MIN_KEY_SIZE:
        raise InvalidKeyException(SMALL_KEY_MSG)

    random_gen = random.SystemRandom()
    byte_array = []
    for _ in range(0, keysize):
        byte_array.append('%c' % random_gen.randrange(0, 255))
    raw_key = ''.join(byte_array)
    return base64.b32encode(raw_key)


def derive_key(master_key, hashfunc=None):
    """
    Derive a secondary key from the user secret key to use in checksum.
    """
    raw_key = base64.b32decode(master_key)
    hashfunc = hashfunc or hashlib.sha1
    return base64.b32encode(hashfunc(raw_key).digest())


def validate_key(input_key):
    try:
        raw_key = base64.b32decode(input_key)
    except TypeError:
        raise InvalidKeyException(INVALID_KEY_MSG)

    if len(raw_key) < MIN_KEY_SIZE:
        raise InvalidKeyException()